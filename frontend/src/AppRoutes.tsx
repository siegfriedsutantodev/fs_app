import {Navigate, Route, Routes} from "react-router-dom"
import Layout from "./layouts/layout";
import Homepage from "./pages/Homepage";
import AuthCallbackPage from "./pages/AuthCallbackPage";

const AppRoutes = () => {
    return(
        <Routes>
            <Route path="/" element={<Layout><Homepage></Homepage></Layout>} />
            <Route path="/auth-callback" element={<AuthCallbackPage />} />
            <Route path="/profile" element={<span>User Profile</span>} />
            <Route path="*" element={<Navigate to="/" />} />
        </Routes>
    )
}

export default AppRoutes;